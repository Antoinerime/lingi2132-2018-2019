.. _part4:

*************************************************************************************************
Partie 4 | Code Generation
*************************************************************************************************

JVM Bytecode proposed by Group 26, Christophe Crochet and Antoine Rime
==========================================================


1. Some theory:
""""""""""""""

1. Why is Java languague machine is named Bytecode ? Explain it briefly
2. It exists different types of machine language. Stack, RISC and CISC. What is the one used by the JVM.
3. Give one advantage of Stack machine compare to RISC architecture.

2. Some Translation:
"""""""""""""""""""

1. Write the Bytecode for this function
2. Give the stack for the the input a = 2

pubic static int inc(int a)
{
  int b = 0;

  for (int i=0;i<a;i++)
  {
    b++;
  }
  return b;
}


Answers:
======================================
1. Theory
""""""""""""""

1. Each instruction is encoded with a fixed number of bytes. For example the imul instruction is encoded as 0110 1000 in the output file.
2. The JVM is a stack machine since it use a stack and each operation operate on it.
3. One advantage of the Stack machine is that, for one instruction on a RISC architecture with 32 registers you need 32*31=992 different opcodes wheras for Stack machine you would only need one.

2. Exercises
"""""""""""""""
Bytecode::

> 0 iconst_0
> 1 istore_2
> 2 iconst_0
> 3 istore_3
> 4 iload_3
> 5 iload_1
> 6 if_icmpge 15
> 9 iinc 2 1
> 12 iinc 3 1
> 15 goto 6
> 16 iload_3
> 17 ireturn
